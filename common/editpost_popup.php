<!-- compose edit post modal -->
<div id="composeeditpostmodal" class="modal compose_tool_box post-popup edit_post_modal main_modal custom_modal compose_edit_modal">
   <div class="hidden_header">
      <div class="content_header">
         <button class="close_span cancel_poup waves-effect">
         <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
         </button>
         <p class="modal_header_xs">Edit post</p>
         <a type="button" class="post_btn action_btn post_btn_xs postbtn savebtn active_post_btn close_modal" onclick="verify()">Save</a>
      </div>
   </div>
   <div class="modal-content">
      <div class="new-post active">
         <div class="top-stuff">
            <div class="postuser-info">
               <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
               <div class="desc-holder">
                  <p class="profile_name">User name</p>
                  <label id="edit_tag_person"></label>
                  <div class="public_dropdown_container">
                      <a class="dropdown_text dropdown-button editpostcreateprivacylabel" onclick="privacymodal(this)" href="javascript:void(0)" data-modeltag="editpostcreateprivacylabel" data-fetch="yes" data-label="editpost">
                        <span id="post_privacy2" class="post_privacy_label">Public</span>
                        <i class="zmdi zmdi-caret-up zmdi-hc-lg"></i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="settings-icon comment_setting_icon">
               <a class="dropdown-button " href="javascript:void(0)" data-activates="new_edit_btn1">
               <i class="zmdi zmdi-more"></i>
               </a>
               <ul id="new_edit_btn1" class="dropdown-content custom_dropdown">
                  <li>
                     <a href="javascript:void(0)">
                     <input type="checkbox" id="toolbox_disable_sharing" />
                     <label for="toolbox_disable_sharing">Disable Sharing</label>
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0)" class="savepost-link">
                     <input type="checkbox" id="toolbox_disable_comments" />
                     <label for="toolbox_disable_comments">Disable Comments</label>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
         <div class="clear"></div>
         <div class="scroll_div">
            <div class="npost-content">
               <div class="post-mcontent">
                  <div class="compose_post_title title_post_container">
                     <input placeholder="Title of this post" id="post_edittitle" class="post_title" type="text" class="validate">
                  </div>
                  <div class="clear"></div>
                  <div class="desc post_comment_box">
                     <textarea id="new_editpost_comment" placeholder="What's new?" class="materialize-textarea comment_textarea"></textarea>
                  </div>
                  <div class="post-photos">
                     <div class="img-row">
                     </div>
                  </div> 
                  <div class="location_parent">
                     <label id="edit_selectedlocation" class="edit_selected_loc"></label>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal-footer">
      <div class="new-post active">
         <div class="post-bcontent">
            <div class="footer_icon_container">
               <button class="comment_footer_icon waves-effect" id="compose_edituploadphotomodalAction">
               <i class="zmdi zmdi-camera"></i>
               </button>
               <button class="comment_footer_icon waves-effect" id="compose_addpersonAction">
               <i class="zmdi zmdi-account"></i>
               </button>
               <button class="comment_footer_icon waves-effect" data-query="all" onfocus="filderMapLocationModal(this)">
               <i class="zmdi zmdi-pin"></i>
               </button>
               <button class="comment_footer_icon compose_titleAction waves-effect" id="compose_edittitleAction">
               <img src="images/addtitleBl.png">
               </button>
            </div>
            <div class="public_dropdown_container_xs">
               <a class="dropdown_text dropdown-button editpostcreateprivacylabel" onclick="privacymodal(this)" href="javascript:void(0)" data-modeltag="editpostcreateprivacylabel" data-fetch="yes" data-label="editpost">
                  <span id="post_privacy2" class="post_privacy_label">Public</span>
                  <i class="zmdi zmdi-caret-up zmdi-hc-lg"></i>
               </a>
            </div>
            <div class="post-bholder">
               <div class="post-loader"><img src="images/home-loader.gif"/></div>
               <div class="hidden_xs">
                  <a class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
                  <a class="btngen-center-align waves-effect">Save</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>